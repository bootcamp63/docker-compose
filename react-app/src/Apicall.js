import React ,{Component}from 'react'

class Apicall extends Component{
    state = {
        data: []
      }

    componentDidMount() {
        this.getData();
    }
    getData= () => {
        const header = new Headers();
        header.append('Access-Control-Allow-Origin', '*');
        header.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        fetch("http://localhost:8080/movies",header).then(response => response.json()).then(response =>{
            console.log(response);
            this.setState({ data: response})
            console.log("****", this.state.data);
         }).catch(err => {
            console.log(err);
          });
    }
        

      render(){
        return  (    
            <div>
               {
                this.state.data.map( (item) =>
                    <li key={item.id}>
                        {item.title}
                    </li> 
                )}
            </div>
        )  
      }
}

export default Apicall;
  